require_relative "spec_helper"
RSpec.describe Cell do
    subject { described_class.new }
    describe "cell" do
        context "created" do
            it "is dead" do
                expect(subject.is_alive).to be false
            end
        end
        context "marked alive" do
            it "is alive" do
                expect { subject.mark_alive }.to change{subject.is_alive}.from(false).to(true)
            end 
        end
    end
end
