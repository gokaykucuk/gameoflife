require_relative "spec_helper"

# Bi sn biseye bakmam lazim
RSpec.describe Agar do


    subject { described_class.new(width:300,height:300) }
    # Bu sekilde 300x300 luk standart bi tane belirleyelim, objeyi test edecegimiz zamanlar 
    # bunun uzerinden yine test edelim
  
  
    describe "agars surface area" do
      # objeyi yaratirken gectigimiz bir parametreyi test ettigimizden subject mantigini sildim su anlik
      # ama yeniden yaratacam :D cunku geri kalan testler icin lazim.
      # burdayim
      # Tmm simdi eger turu cell mi diye test ettik, hepsini de edebilirdik ama birini ettik
      it "has cells as elements of universe array" do
        agar = Agar.new(width:100, height:100)
        expect(agar.universe.first.first).to be_kind_of(Cell)
      end
      
      it "has can create the cells asked" do
        agar = Agar.new(width:100, height:100)
    
        expect(agar.universe.length).to eq(100)
        expect(agar.universe[0].length).to eq(100)
      end
    end

    describe "calls from cells" do
      context "a cell asks for its position" do
        let(:cell_position) { [3,2] }
        let(:cell_to_position) { subject.universe[cell_position[0]][cell_position[1]] }
        it "can tell cells's position" do
          expect(subject.find_position_of_cell(cell_to_position: cell_to_position)).to eq(cell_position)
        end
      end
      context "a cell asks for it's neighbours" do
        let(:cell_to_neighbour) { subject.universe.first.first }
        it "can tell cell's neighbours" do
          expect(subject.neighbours_of_cell(neighbours_of_cell: cell_to_neighbour))
        end
      end


    end
  

  # 2 Example imiz da calisti gayet guzel, simdi daha cok example yapalim.
  
  
    
  
  end