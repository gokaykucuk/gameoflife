class Cell

    attr_reader :is_alive, :agar

    def initialize agar
        @agar = agar
        @is_alive = false
    end

    def mark_alive
        @is_alive = true
    end

    def die
        @is_alive = false
    end
    
    # Any live cell with fewer than two live neighbors dies, as if by underpopulation.
    # okay ama bunlar default olu 
    # aynen oyunu yaratirken bi kismini make alive yapacaz ztn olayimiz o

    # Any live cell with two or three live neighbors lives on to the next generation.
    # Any live cell with more than three live neighbors dies, as if by overpopulation.
    # Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
    # bu tarz? 
    # hayir ama normalde bu olu ya, hani bunu is alive yapan seyi de eklememiz gerekmez mi    
    # hangi kurali diyosun uzerini ciz bi
    # Any live cell with two or three live neighbors lives on to the next generation.
    # tmm eger canliysa bi degisiklik olmuyo, canli kaliyo ztn.
    # eger oluyse de olu kaliyo su an
    def live
        # Bak simdi burda aslinda, alive_neighbours u aliyoruz fln ya.
        # bunu aldigimiz yerde degistiriyoruz ayni anda, o nedenle olen olursa,
        # data o turn bitmeden o etkili oluyo olumuyle.
        # universe arrayining kopyasini cikartip her turnde,
        # o kopya uzerinden okumamiz lazim
        alive_neighbours = agar.alive_neighbours_of_cell(neighbours_of_cell: self)
        if alive_neighbours.count < 2 || alive_neighbours.count > 3 # ikiden az veya  ten coksa olecek
            die
        elsif alive_neighbours.count == 3 # 3 tane varsa canlanacak
            mark_alive
        end
        # tmm aslinda su an game of life bitti :D
        # burdan sonra renderer ve main loop kaldi, onlara bakalim
    end
    # simdi bunlarin speclerini yazalim mi?
    # yoksa renderer mi yapalim?
    # render edelim ben spec yarin kendim yazarim sen bakarsin? onu da kendim yazmayi beceriyim artik :D
    # renderer daha eglenceli olacak :D
    # tmm sen denersin sonra ben bakarim.# tamamdir :D
    # super
    # simdi o zaman TDD den cikiyoruz, direk agile :D
    # once her hucreye kendini render etmesi icin bi firsat taniyalim
    def render
        if is_alive 
            print "+"
        else
            print "#"
        end
    end
    # super :D 
    # boylece kendini render etsin bunlar kare ya da + olarak
    # game e gecelim
end