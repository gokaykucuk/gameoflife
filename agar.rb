require "byebug"
require_relative "cell.rb"
class Agar

  attr_reader :universe

  # boyle icine almamiz gerekmez mi?

  # neyi degistirdin? :D gormedim
  # ha yok su an bisey almayacak bu,
  # kendisini cagirdigimizda evreni yaratacak.
  # bu yani evren sinifi, initialize olunca bos cellerden
  # arrayler yaratacak su an

  # ama mesela sey yapabiliriz aslinda, size i mesela parametrik yapabiliriz?
  # direk 300 de olur
  # hmmm bi tik anlamadim
  # universen icini gezerim Cell.new yapip push ederim ama sikko bi yol bu dimi?
  # biliyorum bi tik   
  # map kullanmayi biliyomusun? yani map yapman daha iyi, fonksiyonel programlamaya yakin
  # yaptigin her sey iyidir :D ondan boyle seylerde map kullan. hatta map ve reduce u ogren baya :D
  # kullanalim orneklerde hep. biliyomusun map ve reduce u? onla yapmayi dene bi. 
  def initialize(width:,height:)
    @universe = Array.new(width) { Array.new(height) { Cell.new(self) }}
     
# abi baya kolaymis bu :D 
# HEHE, arraylerde kisa yol yapmislar map e bildigin. MAp kullanmissin yukarda ztn :D map kullanmak yerine direk
# suslu parantezle giriliyomus hic bilmiyodum :D
# guzel.
# simdi sirada ne var?
# kurallari yapicaz?
# aynen, ama simdi kurallardan once ihtiyacimiz olan bi iki metod var.
# birincisi mesela, bi cell komsularini ogrenebilmeli, cunku ona gore karar verecek.
# bi cell in komsularini ogrenebilmesinin en guzel yolu, agar.rb ye bunu sormak bence, sonucta
# gercek dunyada hayal edince, cell komsularini bilmez ama cell in uzerinde yasadigi evren kesinlikle bilir.
# su laboratuvarlardaki bakterileri fln dusununce :D
# ondan simdi agar.rb yi genisletelim biraz.
# okay

    # @universe = height.times.map do
    #    width.times.map { |_| Cell.new()}
    # end
    # pp @universe
  end

  def give_random_life
    universe.each do |row|
      row.each do |cell|
        cell.mark_alive if Random.rand(1...10) > 3
      end
    end
  end


  # ilk fonksiyon, bi agar in pozisyonunu bulmak olsun.
  # bu nasi bi yazim icine cell mi aliyo
  # evet, ama gonderirken soyle yaziyosun
  # find_position_of_cell(cell: object)
  # ondan kafa karisikligi daha az oluyo
  # duzgun isimlendir degiskenlerini :D daha rahat dusunursun
  # simdi universe in 1. boyutunda gezindiginde ne gececek eline her seferinde
  # bi satir cell
  # cunku birinci boyut yukseklik ikinci boyut en.
  # ondan ben mesela line diye isimlendirirdim
  # .each fonksiyonu yerine, sana indexler gerektigi icin
  # .each_with_index i kullanabilirsin
  def find_position_of_cell cell_to_position:
    universe.each_with_index do |row, row_position|
      # amelelik burasi biraz ondan hizlica yazip geciyorum
      row.each_with_index do |cell, cell_position|
        if cell == cell_to_position
          return [row_position, cell_position]
        end
      end 
    end
  end

  def alive_neighbours_of_cell neighbours_of_cell:
    return [] if neighbours_of_cell(neighbours_of_cell: neighbours_of_cell).nil?
    neighbours_of_cell(neighbours_of_cell: neighbours_of_cell).select{ |cell| cell.is_alive }
  end

  def neighbours_of_cell neighbours_of_cell:
    # Anladin mi konsepti, soyle baslayacak
    cell_position = find_position_of_cell(cell_to_position: neighbours_of_cell)
    # bunu simdi 8 tane niegboru olur ortalardaysa
    neighbours = []
    # evet ama kenarlarin da caresine bakmalisin biraz zorlu bi 
    # olur sen yapabilirsin :D peki :D
    # o bilir ama ben no idea :D
    # yaparim diyo musun yoksa ben mi yaparim? aralarda birakirim sana genel halini sekillendirip?
    # 8 tane komsuyu matematiksel olarak kolayca ifade etmenin bi yolu var mi ki ya, kesin niels bilir bunu
    # distance mantigi da boyle malca bi teknikle calismaz tabi :D
    # neysec
    # bu paso 0 degil mi 
    # ney
    # hee tamam find positionu cagiriyosun oke
    # aynen bunlar hep altyapi calismasi iste :D

    # cell_position[0]
    # neden cell in x deki pozisyonu neyse o o da
    # devam edelim, simdi neigbbours un pozisyonlari olan bi array elde etmis olmaliyiz. bi kontrol edelim
    neighbours << [cell_position[0]-1,cell_position[1]+1]
    neighbours << [cell_position[0],cell_position[1]+1]
    neighbours << [cell_position[0]+1,cell_position[1]+1]
    neighbours << [cell_position[0]-1,cell_position[1]]
    neighbours << [cell_position[0],cell_position[1]]
    neighbours << [cell_position[0]+1,cell_position[1]]
    neighbours << [cell_position[0]-1,cell_position[1]-1]
    neighbours << [cell_position[0],cell_position[1]-1]
    neighbours << [cell_position[0]+1,cell_position[1]-1]
    neighbours.map! { |position| cell_at_position(x: position[0],y: position[1]) }.reject! { |cell| cell.nil? }
    # sanirim yapilabilecek en amele yontemle yaptim
  end
  # neyse simdi sirada ne var?
  # celler artik komsularinin listesini fln alabiliyo ihtiyac duyunca
  # bi game e gidelim?
  def cell_at_position x:, y:
    return nil if x < 0 || y < 0
    universe[x][y]
  rescue
    nil 
  end


  def live
    universe.each { |row| row.each { |cell| cell.live } }
  end
  def render
    # Simdi burda bisey var, eger boylece yazdirirsak butun
    # cell ler yan yana yazdiracak, ama aslinda istedigimiz bu degil.
    # Bu cellerin ayni zamanda satir satir dizilmesini istiyoruz,
    # ondan her cell den sonra alt satira gececz
    universe.each do |row| 
      print "\n"
      row.each { |cell| cell.render }
    end
  end
  # bence de

  # Boyle calisir bence? testini yazalim :D

  # ehehe
  # tmm Simd
  # naapmamiz lazim bi tane universe yaratacaz di mi?
  # bu gelen with ve height yukseklik ve genisliginde bi grid olacak
  # nasil yaparsin bunu benim yaptigimi bosver, sen yap bi

end
